import type { User } from "./User";

export type Photo = {
  id: number
  url: string
  owner: User
}