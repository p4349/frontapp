export type User = {
  name: string
  username: string
  picture: string
}