import type PhotoI from "../Base/Photo";
import type { Photo } from "../../Entities/Photo";
import { PHOTOS_API } from "./static";

export default class PhotoRepository implements PhotoI {

  private parseToModel (response) : Array<Photo> {
    let photos : Photo[];
    photos = response.map(el => ({
      id: el.id,
      url: el.urls.small,
      owner: {
        name: el.user.name,
        username: el.user.username,
        picture: el.user.profile_image.medium
      }
    }))
    return photos;
  }

  getAll (page: number = 1): Promise<Array<Photo>> {
    return fetch(`${PHOTOS_API}/photo?page=${page}`)
      .then(res => res.json())
      .then(res => this.parseToModel(res))
  }

  getToday(): Promise<Array<Photo>> {
    return fetch(`${PHOTOS_API}/photo/today`)
      .then(res => res.json())
      .then(res => this.parseToModel(res))
  }

}