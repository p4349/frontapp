import type { Photo } from "../../Entities/Photo";

export default interface PhotoI {
  getAll: (page: number) => Promise<Array<Photo>>;
  getToday: () => Promise<Array<Photo>>;
}