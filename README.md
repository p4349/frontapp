# Photo Figma

This project it's an exercise to create a [Figma](https://www.figma.com/file/9xMY3e7oPAT8hA93sLIu6m/Prototyping-in-Figma?node-id=203%3A547) Prototype with Svelte.

## Requirements
- nodejs (16.13)

## Instalation
Clone the project and move to the root folder of the project, then execute.
```sh
$ npm i
```
## Enviroment
In order to start with development or production tasks, a file called .env should be created with the following keys.

| NAME | DESCRIPTION |
|--|--|
| API_URL | URL fo the api to use |

Please see the reference file (.env.example).

## Development
> Component development  

When developing new componets, a helpfull command may be.
```bash
$ npm run styleguide
```
> App development  

The following command will start a development server
```bash
$ npm run dev
```

## Production
The following command will create a bundle inside the public directory.
```bash
$ npm run build
```